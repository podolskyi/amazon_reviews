CREATE DATABASE amazon_reviews;

use amazon_reviews;

create table products(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(500),
url VARCHAR(2000)
);

create table reviews(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
reviews_id VARCHAR(20),
product_id int,
review_stars VARCHAR(20),
review_title VARCHAR(400),
reviewer VARCHAR(400),
reviewer_profile_link VARCHAR(300),
review_date VARCHAR(100),
review_text TEXT,
number_helpful VARCHAR(10),
number_total VARCHAR(10),
FOREIGN KEY (product_id) REFERENCES products(id)
);

create table comments(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
review_id INT,
commenter VARCHAR(200),
commenter_profile_link VARCHAR(300),
comment_date VARCHAR(100),
comment_text TEXT,
number_useful VARCHAR(10),
number_total VARCHAR(10),
FOREIGN KEY (review_id) REFERENCES reviews(id)
);


CREATE USER 'python'@'localhost' IDENTIFIED BY 'python';
GRANT ALL PRIVILEGES ON * . * TO 'python'@'localhost';

