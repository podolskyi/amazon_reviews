MySQL

Install:
https://www.digitalocean.com/community/tutorials/a-basic-mysql-tutorial
https://www.digitalocean.com/community/tutorials/mysql-ru

Create DB:
CREATE DATABASE database name;

Del DB:
DROP DATABASE database name;



root password:
P@ssw0rd

------------

CREATE DATABASE amazon_reviews;

use amazon_reviews;

create table products(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(500),
url VARCHAR(2000)
);

create table reviews(id VARCHAR(20) NOT NULL PRIMARY KEY,
product_id int,
review_title VARCHAR(200),
reviewer VARCHAR(200),
review_date VARCHAR(100),
review_text TEXT,
number_helpful VARCHAR(10),
FOREIGN KEY (product_id) REFERENCES products(id)
);

create table comments(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
review_id VARCHAR(20),
commenter VARCHAR(200),
comment_date VARCHAR(100),
comment_text TEXT,
number_people_think VARCHAR(10),
FOREIGN KEY (review_id) REFERENCES reviews(id)
);


drop table comments;

drop table reviews;

drop table products;




CREATE USER 'python'@'localhost' IDENTIFIED BY 'python';
GRANT ALL PRIVILEGES ON * . * TO 'python'@'localhost';


insert into products(name, url) values ('iphone', 'https://philihp.com/blog/2009/how-to-retrieve-the-id-after-a-mysql-insert-in-python/')

insert into reviews (id, product_id, review_title, reviewer, review_date, review_text, number_helpful) values ({0}, {1}, {2}, {3}, {4}, {5}, {6})

insert into comments (review_id, commenter, comment_date, comment_text, number_people_think) values ('{}', '{}', '{}', '{}', '{}')


drop table comments;

drop table reviews;

drop table products;

