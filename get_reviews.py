﻿#!/usr/bin/env python
"""Get Amazon products reviews from URL
"""
import requests
import random
import json
import time
import sys
import csv
import re

import MySQLdb

from bs4 import BeautifulSoup
from optparse import OptionParser

__author__ = "Oleksandr Podolskyi ol.podolskyi@gmail.com"
__email__ = "ol.podolskyi@gmail.com"

def get_ua():
    ua_list = ['Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14',
               'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0',
               'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36',
               'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0']
    ua = random.choice(ua_list)
    return ua

def strp(s):
    s = s.replace("'", "''")
    s = s.replace("\\", " ")
    # s = s.replace("\\x", "\x")
    return s

def write_db(insrt_string, return_id=False):
    db=MySQLdb.connect(host="localhost", user="python", passwd="python", db="amazon_reviews", use_unicode=True, charset="utf8")
    cursor =db.cursor()
    cursor.execute(str(insrt_string))
    if return_id:
        id = cursor.lastrowid
        db.commit()
        db.close()
        return id
    db.commit()
    db.close()


def get_html_page(url):
    headers = {
        'User-Agent': get_ua()
    }
    n = random.random()*5
    time.sleep(n)
    query = requests.get(url, headers=headers)

    return query.text

def get_review_url(prod_url):
    """Returns the URLs of each review of the product."""
    html_page = get_html_page(prod_url)
        
    pat = re.compile(r'<a class="(.*?)" href="(.*?)">See all [\d,]+ customer reviews</a>')
    match = pat.search(html_page)
    url = match.groups()[1]

    # get and write prod
    soup = BeautifulSoup(html_page)
    product_name = soup.find('span', id="productTitle").text

    insert_str_prod = "insert into products(name, url) values ('{0}', '{1}')".format(product_name, prod_url)

    id = write_db(insert_str_prod, return_id=True)

    return (url, id)

def get_reviews(review_url, id):
    """Get reviews data"""
    html_src = get_html_page(review_url)
    soup = BeautifulSoup(html_src)


    reviews_section = soup.find('div', id="cm_cr-review_list")
    reviews_list = reviews_section.find_all('div', class_="a-section review")
    
    parse_reviews(reviews_list, id)

    pagination = soup.find('ul', class_="a-pagination")
    next_page = pagination.find('li', class_="a-last").find('a')
    if next_page:
        print('Next page:', 'http://www.amazon.com' + next_page['href'])
        get_reviews('http://www.amazon.com' + next_page['href'], id)


def parse_reviews(list_reviews, p_id):
    """Parse list review for each page"""
    for num, review in enumerate(list_reviews):
        soup = BeautifulSoup(str(review))
        
        reviews_dict = {}
        
        review_title = soup.find('a', class_="review-title")
        if review_title:
            id = re.search(r'/gp/customer-reviews/([\d\w]+)/', review_title['href']).group(1)
            title = review_title.text
            print('ID:', id)
            print('Title review: ', title)
            reviews_dict['id'] = id 
            reviews_dict['title'] = title
        else:
            reviews_dict['id'] = '-'
            reviews_dict['title'] = '-'

       
        review_author = soup.find('a', class_="author")
        if review_author:
            print('Author:', review_author.text)
            reviews_dict['author'] = review_author.text
        else:
            reviews_dict['author'] = '-'

        review_date = soup.find('span', class_="review-date")
        if review_date:
            date = review_date.text.replace('on ', '')
            print('Date:', date)
            # # print('Date:', review_date.text.replace('on ', ''))
            reviews_dict['date'] = date
        else:
            reviews_dict['date'] = '-'

        review_text = soup.find('span', class_="review-text")
        if review_text:
            print('Text:', review_text.text)
            reviews_dict['review_text'] = review_text.text
        else:
            reviews_dict['review_text'] = '-'

        reviw_helpful = soup.find('span', class_="review-votes")
        if reviw_helpful:
            helpful = re.search(r'(^[\d,]+)', reviw_helpful.text).group(1)
            print('Helpful:', helpful)
            ## print('Helpful:', re.search(r'(^[\d,]+)', reviw_helpful.text).group(1))
            reviews_dict['helpful'] = helpful
        else:
            print('Helpful:', 0)
            reviews_dict['helpful'] = 0

        with open('results.txt', 'a') as f:
            f.write('Title review: ' + reviews_dict['title'] + '\n' +
                'Author: ' + reviews_dict['author'] + '\n' +
                'Date: ' + reviews_dict['date'] + '\n' +
                'Text: ' + reviews_dict['review_text'] + '\n' +
                'Helpful: ' + str(reviews_dict['helpful']) + '\n')

        insert_str_rew = "insert into reviews (id, product_id, review_title, reviewer, review_date, review_text, number_helpful) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')".format(
            reviews_dict['id'],
            p_id, 
            strp(reviews_dict['title']),
            strp(reviews_dict['author']),
            reviews_dict['date'],
            strp(reviews_dict['review_text']),
            reviews_dict['helpful'])
        
        print("+" * 20)
        print(insert_str_rew)
        print("+" * 20)

        write_db(insert_str_rew)

        comments_section = soup.find('div', class_="review-comments").find('span',
            class_="a-expander-prompt")
        comments = comments_section.find('span', class_="a-size-base")
        if comments:
            match = re.search(r'(^[\d,]+) ?', comments.text)
            if match:
                # print('Comments number:', comments.text)
                print('Comments number:', match.group(1))
                get_comments('http://www.amazon.com' + review_title['href'], reviews_dict['id'])
            else:
                print('Comments number:', '-')
        
        del reviews_dict
        
        with open('results.txt', 'a') as f:
            f.write('-----=============================================-----' + '\n')
        print('-----===================================================-----')


def get_comments(url, rew_id):
    """Get comments for each review"""
    html_src = get_html_page(url)
    soup = BeautifulSoup(html_src)

    all_comments = []
    
    comments = soup.find_all('div', class_="postBody")
    all_comments += comments

    pagination = soup.find('div', class_="cdPageSelectorPagination")
    next_page = pagination.find('a', text="Next ›")
    
    while next_page:
        # print('Next com page:', 'http://www.amazon.com' + next_page['href'])
        html_src = get_html_page(next_page['href'])

        soup = BeautifulSoup(html_src)
        comments = soup.find_all('div', class_="postBody")
        all_comments += comments

        pagination = soup.find('div', class_="cdPageSelectorPagination")
        next_page = pagination.find('a', text="Next ›")
        
    parse_comments(all_comments, rew_id)


def parse_comments(comments_list, rew_id):
    """Parse comments for reviews"""
    for num, comment in enumerate(comments_list):
        soup = BeautifulSoup(str(comment))

        postFrom = soup.find('div', class_="postFrom")
        if not postFrom:
            continue
        
        comment_dict = {}

        print('--- Comment number:', num + 1)
        commenter_name = postFrom.find('a')
        if commenter_name:
            print('----- Commenter name:', commenter_name.text)
            comment_dict['commenter_name'] = commenter_name.text
        else:
            comment_dict['commenter_name'] = '-'

        date = soup.find('div', class_="postHeader")
        if date:
            date_ = re.search(r'[\w]{3} [\d]{,2}, [\d]{4} [\d]{,2}:[\d]{,2}:[\d]{,2} (AM|PM)',
                              date.text.strip()).group()

            print('----- Date:', date_)
            comment_dict['date'] = date_
        else:
            comment_dict['date'] = '-'

        content = soup.find('div', class_="postContent")
        if content:
            print('----- Content:', content.text.strip())
            comment_dict['content'] = content.text.strip()
        else:
            comment_dict['content'] = '-'

        num_people_think = soup.find('div', class_="postFooterRight")
        if num_people_think:
            match = re.search(r'(\d+) of \d+ people think this', num_people_think.text)
            if match:
                print('----- People think:', match.group(1))
                comment_dict['like'] = match.group(1)
            else:
                comment_dict['like'] = '-'

        with open('results.txt', 'a') as f:
            f.write('----- Commenter name: ' + comment_dict['commenter_name'] + '\n' +
                '----- Date: ' + comment_dict['date'] + '\n' +
                '----- Content: ' + comment_dict['content'] + '\n' +
                '----- People think: ' + comment_dict['like'] + '\n')

        insert_str_com = "insert into comments (review_id, commenter, comment_date, comment_text, number_people_think) values ('{0}', '{1}', '{2}', '{3}', '{4}')".format(
            rew_id,
            strp(comment_dict['commenter_name']),
            comment_dict['date'],
            strp(comment_dict['content']),
            comment_dict['like'])

        write_db(insert_str_com)


def main():
    """This program can be used to scrape data from Amazon product
    reviews. The default behavior is to process the command line 
    arguments as file with URLs of Amazon products. In this case, the script
    scrapes each of the reviews for the product."""
    # pro_url = "http://www.amazon.com/Plantronics-DA45-USB-Audio-Processor/dp/B000Y9YJK4"
    # product_list = ["http://www.amazon.com/Plantronics-SupraPlus-Wideband-Headset-64338-31/dp/B000LSZ2D6"]
    # pro_url = "http://www.amazon.com/BLU-Advance-Unlocked-Cellphone-White/dp/B00HPTMCRI"

    # parser = argparse.ArgumentParser(
    #     description='The arguments passed in are treated as file with URLs ')
    # parser.add_argument(
    #     '-f', '--file', type=str, help='file with URLs products', required=True)
    # args = parser.parse_args()

    # file_name = args.file

    product_list = ["http://www.amazon.com/Plantronics-DA45-USB-Audio-Processor/dp/B000Y9YJK4", "http://www.amazon.com/Plantronics-SupraPlus-Wideband-Headset-64338-31/dp/B000LSZ2D6"]

    # clear results file
    f_tmp = open('results.txt', 'w')
    f_tmp.close()

    for pro_url in product_list:
        url, id = get_review_url(pro_url)
        print(url)
        get_reviews(url, id)
 

if __name__ == "__main__":
    sys.exit(main())