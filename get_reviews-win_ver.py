﻿# -*- coding: utf-8 -*-
"""Get Amazon products reviews from URL
"""
import requests
import argparse
import logging
import random
import json
import time
import sys
import csv
import re
import os

import MySQLdb

from bs4 import BeautifulSoup
from optparse import OptionParser

__author__ = "Oleksandr Podolskyi"
__email__ = "ol.podolskyi@gmail.com"

def get_ua():
    ua_list = ['Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14',
               'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0',
               'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36',
               'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0',
               'Mozilla/3.0 (Windows NT 4.1; WOW64; rv:25.0) Gecko/201101 Firefox/20.0']
    ua = random.choice(ua_list)
    return ua

def strp(s):
    """Clear text and screening one quate and \\"""
    s = s.replace("'", "''")
    highpoints = re.compile(u'[\U00010000-\U0010ffff]')
    s_ = highpoints.sub(u'', s)
    s = s_.replace("\\", "")
    return s

def write_db(insrt_string, return_id=False):
    try:
        db=MySQLdb.connect(host="127.0.0.1", user="python", passwd="python", db="amazon_reviews",   use_unicode=True, charset="utf8")
        cursor =db.cursor()
        cursor.execute(str(insrt_string))
        if return_id:
            id = cursor.lastrowid
            db.commit()
            db.close()
            return id
        db.commit()
        db.close()
    except:
        ## print("DATABASES error: {0}.".format(sys.exc_info()))
        logging.error("DATABASES error: {0}. Insert string {1}".format(sys.exc_info(), insrt_string))

def get_html_page(get_url):
    headers = {
        'User-Agent': get_ua()
    }
    n = random.random()*5
    time.sleep(n)
    try:
        query = requests.get(get_url, headers=headers)
    except:
        try:
            time.sleep(30)
            query = requests.get(get_url, headers=headers)
        except:
            return None
    html = query.text
    del query
    return html

def get_review_url(prod_url):
    """Returns the URLs of each review of the product."""
    html_page = get_html_page(prod_url)
    if not html_page:
        print('error get reviews product', prod_url)
        logging.error("error get reviews product URL {0}".format(get_url))
        return ('error', None)

    soup = BeautifulSoup(html_page)
        
    pat = re.compile(r'<a class="(.*?)" href="(.*?)">See all [\d,]+ customer reviews</a>')
    match = pat.search(html_page)
    if match:
        url_all_rew = match.groups()[1]
    else:
        all_reviews = soup.find('a', class_="a-link-emphasis a-nowrap")
        if not all_reviews:
            product_name = soup.find('span', id="productTitle").text
            insert_str_prod = "insert into products(name, url) values ('{0}', '{1}')".format(product_name, '-')
            id = write_db(insert_str_prod, return_id=True)
            return ('not_rew', None)
        url_all_rew = all_reviews['href']

    # get and write prod
    product_name = soup.find('span', id="productTitle").text
    insert_str_prod = "insert into products(name, url) values ('{0}', '{1}')".format(product_name, prod_url)
    id = write_db(insert_str_prod, return_id=True)

    return (url_all_rew, id)

def get_reviews(review_url, id):
    """Get reviews data"""
    html_src = get_html_page(review_url)
    if not html_src:
        return None

    soup = BeautifulSoup(html_src)

    reviews_section = soup.find('div', id="cm_cr-review_list")
    if not reviews_section:
        return None
    reviews_list = reviews_section.find_all('div', class_="a-section review")
    if not reviews_list:
        return None
    
    parse_reviews(reviews_list, id)
    print('#')
    
    pagination = soup.find('ul', class_="a-pagination")
    if pagination:
        next_page = pagination.find('li', class_="a-last").find('a')
        if next_page:
            # print('Next page:', 'http://www.amazon.com' + next_page['href'])
            get_reviews('http://www.amazon.com' + next_page['href'], id)

    return True


def parse_reviews(list_reviews, p_id):
    """Parse list review for each page"""
    for num, review in enumerate(list_reviews):
        soup = BeautifulSoup(str(review))
        
        reviews_dict = {}
        
        review_title = soup.find('a', class_="review-title")
        if review_title:
            id = re.search(r'/gp/customer-reviews/([\d\w]+)/', review_title['href']).group(1)
            title = review_title.text
            print('Get review ID:', id)
            print('Get review Title:', title.encode('utf8'))
            reviews_dict['id'] = id 
            reviews_dict['title'] = title
        else:
            reviews_dict['id'] = '-'
            reviews_dict['title'] = '-'

        review_stars = soup.find('span', class_="a-icon-alt")
        if review_stars:
            review_star = re.search(r'([\d].[\d]) out of', str(review_stars)).group(1)
            ## print('review_star:', review_star)
            reviews_dict['review_stars'] = review_star
        else:
            reviews_dict['review_stars'] = 0

        review_author = soup.find('a', class_="author")
        if review_author:
            ## print('Author:', review_author.text.encode('utf8'))
            reviews_dict['author'] = review_author.text
            reviews_dict['reviewer_profile_link'] = 'http://www.amazon.com' + review_author['href']
            ## print('reviewer_profile_link', 'http://www.amazon.com' + review_author['href'])
        else:
            reviews_dict['author'] = '-'
            reviews_dict['reviewer_profile_link'] = '-'

        review_date = soup.find('span', class_="review-date")
        if review_date:
            date = review_date.text.replace('on ', '')
            ## print('Date:', date)
            reviews_dict['date'] = date
        else:
            reviews_dict['date'] = '-'

        review_text = soup.find('span', class_="review-text")
        if review_text:
            ## print('Text:', review_text.text.encode('utf8'))
            reviews_dict['review_text'] = review_text.text
        else:
            reviews_dict['review_text'] = '-'

        reviw_helpful = soup.find('span', class_="review-votes")
        if reviw_helpful:
            helpful = re.search(r'(^[\d,]+) of ([\d,]+)', reviw_helpful.text)
            ## print('Helpful:', helpful.group(1))
            ## print('number_total:', helpful.group(2))
            reviews_dict['helpful'] = helpful.group(1)
            reviews_dict['number_total'] = helpful.group(2)
        else:
            ## print('Helpful:', 0)
            reviews_dict['helpful'] = 0
            reviews_dict['number_total'] = 0

        """
        with open('results.txt', 'a') as f:
            f.write('Title review: ' + reviews_dict['title'] + '\n' +
                'Author: ' + reviews_dict['author'] + '\n' +
                'Date: ' + reviews_dict['date'] + '\n' +
                'Text: ' + reviews_dict['review_text'] + '\n' +
                'Helpful: ' + str(reviews_dict['helpful']) + '\n')
        """

        insert_str_rew = "insert into reviews (reviews_id, product_id, review_stars, review_title, reviewer, reviewer_profile_link, review_date, review_text, number_helpful, number_total) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')".format(
            reviews_dict['id'],
            p_id, 
            strp(reviews_dict['review_stars']),
            strp(reviews_dict['title']),
            strp(reviews_dict['author']),
            strp(reviews_dict['reviewer_profile_link']),
            reviews_dict['date'],
            strp(reviews_dict['review_text']),
            reviews_dict['helpful'],
            reviews_dict['number_total'])

        id_rew = write_db(insert_str_rew, return_id=True)

        comments_section_ = soup.find('div', class_="review-comments")
        if comments_section_:
            comments_section = comments_section_.find('span', class_="a-expander-prompt")
            if comments_section:
                comments = comments_section.find('span', class_="a-size-base")
                if comments:
                    match = re.search(r'(^[\d,]+) ?', comments.text)
                    if match:
                        ## print('Comments number:', match.group(1))
                        get_comments('http://www.amazon.com' + review_title['href'], id_rew)
        
        del reviews_dict
        ## print('-----===================================================-----')


def get_comments(comment_url, rew_id):
    """Get comments for each review"""
    html_src = get_html_page(comment_url)
    if not html_src:
        return
    soup = BeautifulSoup(html_src)

    all_comments = []
    
    comments = soup.find_all('div', class_="postBody")
    if not comments:
        return
    all_comments += comments

    pagination = soup.find('div', class_="cdPageSelectorPagination")
    if pagination:
        next_page = pagination.find('a', text="Next ›")
        
        while next_page:
            html_src = get_html_page(next_page['href'])
    
            soup = BeautifulSoup(html_src)
            comments = soup.find_all('div', class_="postBody")
            all_comments += comments
    
            pagination = soup.find('div', class_="cdPageSelectorPagination")
            next_page = pagination.find('a', text="Next ›")
        
    parse_comments(all_comments, rew_id)
    del all_comments


def parse_comments(comments_list, rew_id):
    """Parse comments for reviews"""
    for num, comment in enumerate(comments_list):
        soup = BeautifulSoup(str(comment))

        postFrom = soup.find('div', class_="postFrom")
        if not postFrom:
            return
        
        comment_dict = {}

        ## print('--- Comment number:', num + 1)
        commenter_name = postFrom.find('a')
        if commenter_name:
            ## print('----- Commenter name:', commenter_name.text.encode('utf8'))
            ## print('----- Commenter_profile_link:', commenter_name['href'])
            comment_dict['commenter_name'] = commenter_name.text
            comment_dict['commenter_profile_link'] = commenter_name['href']
        else:
            comment_dict['commenter_name'] = '-'
            comment_dict['commenter_profile_link'] = '-'

        date = soup.find('div', class_="postHeader")
        if date:
            date_ = re.search(r'[\w]{3} [\d]{,2}, [\d]{4} [\d]{,2}:[\d]{,2}:[\d]{,2} (AM|PM)',
                              date.text.strip()).group()

            ## print('----- Date:', date_)
            comment_dict['date'] = date_
        else:
            comment_dict['date'] = '-'

        content = soup.find('div', class_="postContent")
        if content:
            ## print('----- Content:', content.text.strip().encode('utf8'))
            comment_dict['content'] = content.text.strip()
        else:
            comment_dict['content'] = '-'

        num_people_think = soup.find('div', class_="postFooterRight")
        if num_people_think:
            match = re.search(r'(\d+) of (\d+) people think this', num_people_think.text)
            if match:
                ## print('----- People think:', match.group(1).encode('utf8'))
                comment_dict['number_useful'] = match.group(1)
                comment_dict['number_total'] = match.group(2)
            else:
                comment_dict['number_useful'] = 0
                comment_dict['number_total'] = 0

        """
        with open('results.txt', 'a') as f:
            f.write('----- Commenter name: ' + comment_dict['commenter_name'].encode('utf8') + '\n' +
                '----- Date: ' + comment_dict['date'] + '\n' +
                '----- Content: ' + comment_dict['content'].encode('utf8') + '\n' +
                '----- People think: ' + comment_dict['like'].encode('utf8') + '\n')
        """

        insert_str_com = "insert into comments (review_id, commenter, commenter_profile_link, comment_date, comment_text, number_useful, number_total) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')".format(
            rew_id,
            strp(comment_dict['commenter_name']),
            strp(comment_dict['commenter_profile_link']),
            comment_dict['date'],
            strp(comment_dict['content']),
            comment_dict['number_useful'],
            comment_dict['number_total'])

        del comment_dict

        write_db(insert_str_com)


def main():
    """This program can be used to scrape data from Amazon product
    reviews. The default behavior is to process the command line 
    arguments as file with URLs of Amazon products. In this case, the script
    scrapes each of the reviews for the product."""

    # init loggin file
    local_dir = os.path.dirname(os.path.abspath(__file__))

    logging.basicConfig(filename=local_dir + '/get_amazon_reviews.log',
                        format='%(lineno)d %(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        level=logging.INFO)
    logging.info('--------------------------------------------------')
    logging.info('--------------------------------------------------')
    logging.info('Scraping started')

    parser = argparse.ArgumentParser(
        description='The arguments passed in are treated as file with URLs ')
    parser.add_argument(
        '-f', '--file', type=str, help='file with URLs products', required=True)
    args = parser.parse_args()

    file_name = args.file

    ## for test
    ## product_list = ["http://www.amazon.com/Samsung-Galaxy-S7262-Unlocked-Cellphone/dp/B00IT3WYQS/", "http://www.amazon.com/BLU-Advance-4-0L-Unlocked-Smartphone/dp/B00YCTIL88", "http://www.amazon.com/BLU-Advance-4-0L-Unlocked-Smartphone/dp/B00YCTIPEI", "http://www.amazon.com/Advance-Etubby-Fashional-Protective-Magnetic/dp/B010I8YQNK", "http://www.amazon.com/dp/B014LVHIBC"]

    product_list = []
    
    with open(file_name, 'r') as furl:
        for line in furl:
            product_list.append(line.rstrip())

    for pro_url in product_list:
        url, id = get_review_url(pro_url)
        if url == 'not_rew':
            print('The product has not reviews. url:', pro_url)
            logging.info("The product has no reviews, product URL: {0}".format(pro_url))
            continue
        elif url == 'error':
            continue
        print('Get reviews for:', url)
        res = get_reviews(url, id)
        if not res:
            print('Error get reviews page or reviews block. review url:', url)
            logging.info("Error get reviews page or reviews block. review url: {0}".format(url))
            continue

 

if __name__ == "__main__":
    sys.exit(main())
